package pl.kkosobudzki.butterknife.app;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author Krzysztof Kosobudzki
 */
public class MainActivity extends Activity {
    @InjectView(R.id.sample_edit_text) EditText mSampleEditText;
    @InjectView(R.id.sample_hello_button) Button mSampleHelloButton;
    @InjectView(R.id.sample_goodbye_button) Button mSampleGoodbyeButton;

    private List<Button> mButtonsList;

    static final ButterKnife.Action<View> TOGGLE_ENABLED = new ButterKnife.Action<View>() {
        @Override
        public void apply(View view, int i) {
            view.setEnabled(!view.isEnabled());
        }
    };

    static final ButterKnife.Setter<View, Boolean> ENABLED = new ButterKnife.Setter<View, Boolean>() {
        @Override
        public void set(View view, Boolean enabled, int i) {
            view.setEnabled(enabled);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ButterKnife.inject(this);

        mButtonsList = Arrays.asList(mSampleHelloButton, mSampleGoodbyeButton);

        ButterKnife.apply(mButtonsList, ENABLED, false);
    }

    @OnTextChanged(R.id.sample_edit_text)
    public void onNameChanged(CharSequence text) {
        if (TextUtils.isEmpty(text)) {
            ButterKnife.apply(mButtonsList, ENABLED, false);
        } else {
            mSampleHelloButton.setEnabled(true);
            mSampleGoodbyeButton.setEnabled(false);
        }
    }

    @OnClick(R.id.sample_hello_button)
    public void sayHello() {
        if (TextUtils.isEmpty(mSampleEditText.getText().toString())) {
            Toast.makeText(this, R.string.hello_empty, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(
                    this,
                    String.format(getString(R.string.hello), mSampleEditText.getText().toString()),
                    Toast.LENGTH_SHORT
            ).show();

            ButterKnife.apply(mButtonsList, TOGGLE_ENABLED);
        }
    }

    @OnClick(R.id.sample_goodbye_button)
    public void sayGoodbye() {
        Toast.makeText(this, R.string.goodbye, Toast.LENGTH_SHORT).show();

        ButterKnife.apply(mButtonsList, TOGGLE_ENABLED);

        mSampleEditText.setText(null);
    }
}
